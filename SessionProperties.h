/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

//! Allow Item Flags
enum AllowItemFlags
{
  AIF_HEALTH    = (1L << 0),
  AIF_ARMOR     = (1L << 1),
  AIF_SHIELDS   = (1L << 2),
  AIF_WEAPONS   = (1L << 3),
  AIF_AMMO      = (1L << 4),
  AIF_AMMOPACKS = (1L << 5),
  AIF_POWERUPS  = (1L << 6),
  AIF_GADGETS   = (1L << 7),
};

//! Cooperative Flags
enum CooperativeFlags
{
  CPF_PLAY_ENTIRE_GAME  = (1L << 0),
  CPF_RESPAWN_IN_PLACE  = (1L << 1),
  CPF_USE_LIVES         = (1L << 2),
  CPF_SHARED_LIVES      = (1L << 3),
  CPF_SHARED_KEYS       = (1L << 4),
  CPF_HEALTH_STAYS      = (1L << 5),
  CPF_ARMOR_STAYS       = (1L << 6),
  CPF_AMMO_STAYS        = (1L << 7),
  CPF_WEAPONS_STAY      = (1L << 8),
  CPF_USE_EXTRA_ENEMIES = (1L << 9),
};

//! Versus Flags
enum VersusFlags
{
  VSF_AUTO_RESPAWN = (1L << 0),
  VSF_DROP_WEAPON  = (1L << 1),
  VSF_DROP_POWERUP = (1L << 2),
};

/*
 * Class responsible for describing game session
 */
class CSessionProperties : public CResource
{
  public:
    enum GameMode {
      GM_FLYOVER = -1,
      GM_COOPERATIVE = 0,
      GM_SCOREMATCH,
      GM_FRAGMATCH,

      GM_LASTGAMEMODE,
      GM_TEAMDEATHMATCH,
    };

    enum GameDifficulty {
      GD_TOURIST = -1,
      GD_EASY = 0,
      GD_NORMAL,
      GD_HARD,
      GD_EXTREME,
    };

    // General
    INDEX sp_ctMaxPlayers;    // maximum number of players in game
    BOOL sp_bWaitAllPlayers;  // wait for all players to connect
    BOOL sp_bQuickTest;       // set when game is tested from wed
    BOOL sp_bCooperative;     // players are not intended to kill each other
    BOOL sp_bSinglePlayer;    // single player mode has some special rules
    BOOL sp_bVersus;
    BOOL sp_bUseFrags;        // set if frags matter instead of score

    enum GameMode sp_gmGameMode;    // general game rules

    enum GameDifficulty sp_gdGameDifficulty;
    ULONG sp_ulSpawnFlags;
    BOOL sp_bMental;            // set if mental mode engaged

    // Limits
    INDEX sp_iScoreLimit;       // stop game after a player/team reaches given score
    INDEX sp_iFragLimit;        // stop game after a player/team reaches given score
    INDEX sp_iGoalLimit;
    INDEX sp_iTimeLimit;        // stop game after given number of minutes elapses

    BOOL sp_bTeamPlay;          // players are divided in teams
    BOOL sp_bFriendlyFire;      // can harm player of same team
    BOOL sp_bInfiniteAmmo;      // ammo is not consumed when firing

    // Difficulty
    FLOAT sp_fEnemyMovementSpeed; // enemy speed multiplier
    FLOAT sp_fEnemyAttackSpeed;   // enemy speed multiplier
    FLOAT sp_fDamageStrength;     // multiplier when damaged
    FLOAT sp_fAmmoQuantity;       // multiplier for ammo capacity
    FLOAT sp_fAmmoPickupQuantity;       // multiplier when picking up ammo
    FLOAT sp_fExtraEnemyStrength;            // fixed adder for extra enemy power
    FLOAT sp_fExtraEnemyStrengthPerPlayer;   // adder for extra enemy power per each player playing

    FLOAT sp_tmSpawnInvulnerability;   // how many seconds players are invunerable after respawning

    BOOL  sp_bEndOfGame;     // marked when dm game is finished (any of the limits reached)

    ULONG sp_ulLevelsMask;    // mask of visited levels so far

    // Common
    BOOL  sp_bCustomizedGame;
    ULONG sp_ulAllowedItemTypes;

    // Cooperative
    ULONG sp_ulCoopFlags;
    INDEX sp_ctInitialLives;
    INDEX sp_ctInitialScoreForLife;

    // Versus
    ULONG sp_ulVersusFlags;
    FLOAT sp_tmRespawnDelay;

    // Scorematch
    FLOAT sp_fManaTransferFactor; // multiplier for the killed player mana that is to be added to killer's mana
    INDEX sp_iInitialMana;        // life price (mana that each player'll have upon respawning)

    // Blood & Gore
    INDEX sp_iBlood;         // blood/gibs type (0=none, 1=green, 2=red, 3=hippie)
    BOOL  sp_bGibs;          // enable/disable gibbing
  
  public:
    //! Constructor.
    CSessionProperties();
  
    //! Copy constructor.
    CSessionProperties(const CSessionProperties& other);
    
    //! Destructor.
    virtual ~CSessionProperties();

    //! Copy all the data.
    void Copy(const CGameObject &other);
    
    //! Get exact copy of the object.
    virtual CGameObject *Clone() const;

    //! Free memory.
    void Clear();

    //! Read model instance from stream.
    void Read_t(CTStream *istrFile);  // throw char *

    //! Write model instance in stream.
    void Write_t(CTStream *ostrFile); // throw char *

    //! Get the resource type.
    virtual UBYTE GetType() const;
};
