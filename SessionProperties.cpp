﻿/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdAfx.h"
#include "Game.h"
#include "SessionPropertiesFactory.h"

extern CTString gam_strGameAgentExtras;

// General
extern INDEX gam_ctMaxPlayers = 8;
extern INDEX gam_bWaitAllPlayers = FALSE;
extern INDEX gam_iQuickStartDifficulty = 1;
extern INDEX gam_iQuickStartMode = 0;
extern INDEX gam_bQuickStartMP = 0;
extern INDEX gam_iStartDifficulty = 1;
extern INDEX gam_iStartMode = 0;

// Scorematch
extern INDEX gam_iInitialMana = 100;
extern FLOAT gam_fManaTransferFactor = 0.5f;

// Difficulty
extern FLOAT gam_afAmmoQuantity[5]        = {4.0f,  4.0f,  2.0f, 2.0f , 4.0f };
extern FLOAT gam_afAmmoPickupQuantity[5]  = {2.0f,  2.0f,  1.0f, 1.0f , 2.0f };
extern FLOAT gam_afDamageStrength[5]      = {0.25f, 0.5f,  1.0f, 1.5f , 2.0f };
extern FLOAT gam_afEnemyAttackSpeed[5]    = {0.75f, 0.75f, 1.0f, 2.0f , 2.0f };
extern FLOAT gam_afEnemyMovementSpeed[5]  = {1.0f , 1.0f , 1.0f, 1.25f, 1.25f};
extern FLOAT gam_fExtraEnemyStrength = 0.0f;
extern FLOAT gam_fExtraEnemyStrengthPerPlayer = 0.0f;

// Versus/Teamplay Limits
extern INDEX gam_iScoreLimit = 100000;
extern INDEX gam_iFragLimit = 20;
extern INDEX gam_iGoalLimit = 3; // Capture Objectives
extern INDEX gam_iTimeLimit = 0;

// Common Flags
extern INDEX gam_bCustomizedGame = TRUE;

extern INDEX gam_bAllowHealthItems = TRUE;
extern INDEX gam_bAllowArmorItems = TRUE;
extern INDEX gam_bAllowWeaponItems = TRUE;
extern INDEX gam_bAllowAmmoItems = TRUE;
extern INDEX gam_bAllowAmmoPackItems = TRUE;
extern INDEX gam_bAllowPowerUpItems = TRUE;
extern INDEX gam_bAllowGadgetItems = TRUE;

extern INDEX gam_bInfiniteAmmo = FALSE;
extern INDEX gam_bFriendlyFire = FALSE;

// Common Options
extern FLOAT gam_tmRespawnDelay = 0.0F;
extern FLOAT gam_tmRespawnInvulnerability = 3.0f;

// Cooperative Flags
extern INDEX gam_bPlayEntireGame = TRUE;
extern INDEX gam_bRespawnInPlace = TRUE;
extern INDEX gam_bUseLives = TRUE;
extern INDEX gam_bSharedKeys = TRUE;
extern INDEX gam_bSharedLives = TRUE;
extern INDEX gam_bHealthStays = TRUE;
extern INDEX gam_bArmorStays = TRUE;
extern INDEX gam_bAmmoStays = TRUE;
extern INDEX gam_bWeaponsStay = TRUE;
extern INDEX gam_bUseExtraEnemies = TRUE;

// Cooperative Options
extern INDEX gam_iInitialLives = -1;
extern INDEX gam_iInitialScoreForLife = 100000;

// Versus Flags
extern INDEX gam_bAutoRespawn = TRUE;
extern INDEX gam_bDropWeapon = TRUE;
extern INDEX gam_bDropPowerUp = TRUE;

// Blood & Gore
extern INDEX gam_iBlood = 2; // 0=none, 1=green, 2=red, 3=hippie
extern INDEX gam_bGibs = TRUE;

static BOOL IsVersusGamemode(enum CSessionProperties::GameMode eGameMode)
{
  switch (eGameMode)
  {
    case CSessionProperties::GM_FRAGMATCH: return TRUE;
    case CSessionProperties::GM_SCOREMATCH: return TRUE;
    case CSessionProperties::GM_TEAMDEATHMATCH: return TRUE;
  }

  return FALSE;
}

static void SetGameModeParameters(CSessionProperties &sp)
{
  sp.sp_gmGameMode = (CSessionProperties::GameMode) Clamp(INDEX(gam_iStartMode), -1L, 2L);

  switch (sp.sp_gmGameMode)
  {
    default: {
      ASSERT(FALSE);
    }

    case CSessionProperties::GM_COOPERATIVE: {
      sp.sp_ulSpawnFlags |= SPF_SINGLEPLAYER|SPF_COOPERATIVE;
    } break;

    case CSessionProperties::GM_FLYOVER: {
      sp.sp_ulSpawnFlags |= SPF_FLYOVER|SPF_MASK_DIFFICULTY;
    } break;

    case CSessionProperties::GM_SCOREMATCH:
    case CSessionProperties::GM_FRAGMATCH: {
      sp.sp_ulSpawnFlags |= SPF_DEATHMATCH;
    } break;
  }
}
static void SetDifficultyParameters(CSessionProperties &sp)
{
  INDEX iDifficulty = gam_iStartDifficulty;

  if (iDifficulty == 4) {
    sp.sp_bMental = TRUE;
    iDifficulty = 2;

  } else {
    sp.sp_bMental = FALSE;
  }

  sp.sp_gdGameDifficulty = (CSessionProperties::GameDifficulty)Clamp(INDEX(iDifficulty), -1L, 3L);

  switch (sp.sp_gdGameDifficulty)
  {
    case CSessionProperties::GD_TOURIST: {
      sp.sp_ulSpawnFlags = SPF_EASY;//SPF_TOURIST; !!!!
      sp.sp_fEnemyMovementSpeed = gam_afEnemyMovementSpeed[0];
      sp.sp_fEnemyAttackSpeed   = gam_afEnemyAttackSpeed  [0];
      sp.sp_fDamageStrength     = gam_afDamageStrength    [0];
      sp.sp_fAmmoQuantity       = gam_afAmmoQuantity      [0];
      sp.sp_fAmmoPickupQuantity = gam_afAmmoPickupQuantity[0];
    } break;

    case CSessionProperties::GD_EASY: {
      sp.sp_ulSpawnFlags = SPF_EASY;
      sp.sp_fEnemyMovementSpeed = gam_afEnemyMovementSpeed[1];
      sp.sp_fEnemyAttackSpeed   = gam_afEnemyAttackSpeed  [1];
      sp.sp_fDamageStrength     = gam_afDamageStrength    [1];
      sp.sp_fAmmoQuantity       = gam_afAmmoQuantity      [1];
      sp.sp_fAmmoPickupQuantity = gam_afAmmoPickupQuantity[1];
    } break;

    default: {
      ASSERT(FALSE);
    }

    case CSessionProperties::GD_NORMAL: {
      sp.sp_ulSpawnFlags = SPF_NORMAL;
      sp.sp_fEnemyMovementSpeed = gam_afEnemyMovementSpeed[2];
      sp.sp_fEnemyAttackSpeed   = gam_afEnemyAttackSpeed  [2];
      sp.sp_fDamageStrength     = gam_afDamageStrength    [2];
      sp.sp_fAmmoQuantity       = gam_afAmmoQuantity      [2];
      sp.sp_fAmmoPickupQuantity = gam_afAmmoPickupQuantity[2];
    } break;

    case CSessionProperties::GD_HARD: {
      sp.sp_ulSpawnFlags = SPF_HARD;
      sp.sp_fEnemyMovementSpeed = gam_afEnemyMovementSpeed[3];
      sp.sp_fEnemyAttackSpeed   = gam_afEnemyAttackSpeed  [3];
      sp.sp_fDamageStrength     = gam_afDamageStrength    [3];
      sp.sp_fAmmoQuantity       = gam_afAmmoQuantity      [3];
      sp.sp_fAmmoPickupQuantity = gam_afAmmoPickupQuantity[3];
    } break;

    case CSessionProperties::GD_EXTREME: {
      sp.sp_ulSpawnFlags = SPF_EXTREME;
      sp.sp_fEnemyMovementSpeed = gam_afEnemyMovementSpeed[4];
      sp.sp_fEnemyAttackSpeed   = gam_afEnemyAttackSpeed  [4];
      sp.sp_fDamageStrength     = gam_afDamageStrength    [4];
      sp.sp_fAmmoQuantity       = gam_afAmmoQuantity      [4];
      sp.sp_fAmmoPickupQuantity = gam_afAmmoPickupQuantity[4];
    } break;
  }
}

// set properties for a single player session
void CGame::SetSinglePlayerSession(CGameObject *pProperties)
{
  CSessionProperties& sp = *static_cast<CSessionProperties*>(pProperties);

  // clear
  sp.Clear();

  SetDifficultyParameters(sp);
  SetGameModeParameters(sp);
  sp.sp_ulSpawnFlags &= ~SPF_COOPERATIVE;

  sp.sp_bEndOfGame = FALSE;

  sp.sp_ctMaxPlayers = 1;
  sp.sp_bWaitAllPlayers = FALSE;
  sp.sp_bQuickTest = FALSE;
  sp.sp_bCooperative = TRUE;
  sp.sp_bSinglePlayer = TRUE;
  sp.sp_bUseFrags = FALSE;

  // Limits
  sp.sp_iScoreLimit = 0;
  sp.sp_iFragLimit = 0;
  sp.sp_iGoalLimit = 0;
  sp.sp_iTimeLimit = 0;

  sp.sp_tmSpawnInvulnerability = 0;

  sp.sp_bTeamPlay = FALSE;
  sp.sp_bFriendlyFire = FALSE;

  sp.sp_bInfiniteAmmo = FALSE;
  sp.sp_fExtraEnemyStrength = 0;
  sp.sp_fExtraEnemyStrengthPerPlayer = 0;

  sp.sp_iBlood = Clamp(gam_iBlood, 0L, 3L);
  sp.sp_bGibs = gam_bGibs;

  sp.sp_ulAllowedItemTypes = 0xFFFFFFFF;
  sp.sp_ctInitialLives = 0;
  sp.sp_ctInitialScoreForLife = 0;
}

// set properties for a quick start session
void CGame::SetQuickStartSession(CGameObject *pProperties)
{
  CSessionProperties& sp = *static_cast<CSessionProperties*>(pProperties);

  gam_iStartDifficulty = gam_iQuickStartDifficulty;
  gam_iStartMode = gam_iQuickStartMode;

  // same as single player
  if (!gam_bQuickStartMP) {
    SetSinglePlayerSession(pProperties);

  } else {
    SetMultiPlayerSession(pProperties);
  }

  // quick start type
  sp.sp_bQuickTest = TRUE;
}

// set properties for a multiplayer session
void CGame::SetMultiPlayerSession(CGameObject *pProperties)
{
  CSessionProperties& sp = *static_cast<CSessionProperties*>(pProperties);

  // clear
  sp.Clear();

  SetDifficultyParameters(sp);
  SetGameModeParameters(sp);
  
  if (IsVersusGamemode(sp.sp_gmGameMode)) {
    sp.sp_fAmmoQuantity = 1.0F;
  }
  
  sp.sp_ulSpawnFlags &= ~SPF_SINGLEPLAYER;

  sp.sp_bCustomizedGame = gam_bCustomizedGame;

  sp.sp_bEndOfGame = FALSE;

  sp.sp_bQuickTest = FALSE;
  sp.sp_bCooperative = sp.sp_gmGameMode == CSessionProperties::GM_COOPERATIVE;
  sp.sp_bVersus = IsVersusGamemode(sp.sp_gmGameMode);
  sp.sp_bSinglePlayer = FALSE;
  sp.sp_bUseFrags = sp.sp_gmGameMode == CSessionProperties::GM_FRAGMATCH;
  sp.sp_bFriendlyFire = gam_bFriendlyFire;
  sp.sp_ctMaxPlayers = gam_ctMaxPlayers;
  sp.sp_bWaitAllPlayers = gam_bWaitAllPlayers;

  sp.sp_bInfiniteAmmo     = gam_bInfiniteAmmo;

  sp.sp_fManaTransferFactor = gam_fManaTransferFactor;
  sp.sp_fExtraEnemyStrength = gam_fExtraEnemyStrength;
  sp.sp_fExtraEnemyStrengthPerPlayer = gam_fExtraEnemyStrengthPerPlayer;
  sp.sp_iInitialMana = gam_iInitialMana;

  sp.sp_tmSpawnInvulnerability = gam_tmRespawnInvulnerability;

  sp.sp_ulAllowedItemTypes = 0xFFFFFFFF;

  sp.sp_ulCoopFlags |= CPF_PLAY_ENTIRE_GAME;

  // Common: Allow/Disallow items.
  if (sp.sp_bCustomizedGame) {
    sp.sp_ulAllowedItemTypes = 0;
    sp.sp_ulAllowedItemTypes |= gam_bAllowHealthItems ? AIF_HEALTH : 0;
    sp.sp_ulAllowedItemTypes |= gam_bAllowArmorItems ? AIF_ARMOR : 0;
    sp.sp_ulAllowedItemTypes |= gam_bAllowWeaponItems ? AIF_WEAPONS : 0;
    sp.sp_ulAllowedItemTypes |= gam_bAllowAmmoItems ? AIF_AMMO : 0;
    sp.sp_ulAllowedItemTypes |= gam_bAllowAmmoPackItems ? AIF_AMMOPACKS : 0;
    sp.sp_ulAllowedItemTypes |= gam_bAllowPowerUpItems ? AIF_POWERUPS : 0;
    sp.sp_ulAllowedItemTypes |= gam_bAllowGadgetItems ? AIF_GADGETS : 0;
  }

  if (sp.sp_bCooperative && sp.sp_bCustomizedGame) {
    sp.sp_ulCoopFlags = 0;
    sp.sp_ulCoopFlags |= gam_bPlayEntireGame ? CPF_PLAY_ENTIRE_GAME : 0;
    sp.sp_ulCoopFlags |= gam_bRespawnInPlace ? CPF_RESPAWN_IN_PLACE : 0;
    sp.sp_ulCoopFlags |= gam_bUseLives ? CPF_USE_LIVES : 0;
    sp.sp_ulCoopFlags |= gam_bSharedLives ? CPF_SHARED_LIVES : 0;
    sp.sp_ulCoopFlags |= gam_bSharedKeys ? CPF_SHARED_KEYS : 0;
    sp.sp_ulCoopFlags |= gam_bHealthStays ? CPF_HEALTH_STAYS : 0;
    sp.sp_ulCoopFlags |= gam_bArmorStays ? CPF_ARMOR_STAYS : 0;
    sp.sp_ulCoopFlags |= gam_bAmmoStays ? CPF_AMMO_STAYS : 0;
    sp.sp_ulCoopFlags |= gam_bWeaponsStay ? CPF_WEAPONS_STAY : 0;
    sp.sp_ulCoopFlags |= gam_bUseExtraEnemies ? CPF_USE_EXTRA_ENEMIES : 0;
  }

  if (sp.sp_bVersus && sp.sp_bCustomizedGame) {
    sp.sp_ulVersusFlags = 0;
    sp.sp_ulVersusFlags |= gam_bAutoRespawn ? VSF_AUTO_RESPAWN : 0;
    sp.sp_ulVersusFlags |= gam_bDropWeapon ? VSF_DROP_WEAPON : 0;
    sp.sp_ulVersusFlags |= gam_bDropPowerUp ? VSF_DROP_POWERUP : 0;
  }

  // set credits and limits
  if (sp.sp_bCooperative) {
    sp.sp_ctInitialLives = gam_iInitialLives;
    sp.sp_ctInitialScoreForLife = gam_iInitialScoreForLife;
    sp.sp_iScoreLimit = 0;
    sp.sp_iFragLimit = 0;
    sp.sp_iGoalLimit = 0;
    sp.sp_iTimeLimit = 0;

  } else {
    sp.sp_ctInitialLives = -1;
    sp.sp_ctInitialScoreForLife = -1;
    sp.sp_iScoreLimit = gam_iScoreLimit;
    sp.sp_iFragLimit = gam_iFragLimit;
    sp.sp_iGoalLimit = gam_iGoalLimit;
    sp.sp_iTimeLimit = gam_iTimeLimit;

    if (sp.sp_bUseFrags) {
      sp.sp_iScoreLimit = 0;
    } else {
      sp.sp_iFragLimit = 0;
    }
  }

  // Blood & Gore
  sp.sp_iBlood = Clamp( gam_iBlood, 0L, 3L);
  sp.sp_bGibs = gam_bGibs;
}

BOOL IsMenuEnabled(const CTString &strMenuName)
{
  if (strMenuName == "Single Player") {
    return TRUE;

  } else if (strMenuName == "Network") {
    return TRUE;

  } else if (strMenuName == "Split Screen") {
    return TRUE;

  } else if (strMenuName == "High Score") {
    return TRUE;

  } else if (strMenuName == "Training") {
    return FALSE;

  } else if (strMenuName == "Technology Test") {
    return TRUE;

  }

  return TRUE;
}

BOOL IsMenuEnabledCfunc(void* pArgs)
{
  CTString strMenuName = *NEXTARGUMENT(CTString*);
  return IsMenuEnabled(strMenuName);
}

CTString GetGameTypeName(INDEX iMode)
{
  switch (iMode)
  {
    default: {
      return "";
    } break;

    case CSessionProperties::GM_COOPERATIVE: {
      return TRANS("Cooperative");
    } break;

    case CSessionProperties::GM_FLYOVER: {
      return TRANS("Flyover");
    } break;

    case CSessionProperties::GM_SCOREMATCH: {
      return TRANS("Scorematch");
    } break;

    case CSessionProperties::GM_FRAGMATCH: {
      return TRANS("Fragmatch");
    } break;

    case CSessionProperties::GM_TEAMDEATHMATCH: {
      return TRANS("TDM");
    } break;
  }
}

CTString GetGameTypeNameCfunc(void* pArgs)
{
  INDEX iMode = NEXTARGUMENT(INDEX);
  return GetGameTypeName(iMode);
}

CTString GetCurrentGameTypeName()
{
  const CSessionProperties &sp = *GetSP();
  return GetGameTypeName(sp.sp_gmGameMode);
}

CTString GetGameAgentRulesInfo(void)
{
  CTString strOut;
  CTString strKey;
  const CSessionProperties &sp = *GetSP();

  CTString strDifficulty;

  if (sp.sp_bMental) {
    strDifficulty = TRANS("Mental");

  } else {
    switch(sp.sp_gdGameDifficulty)
    {
      case CSessionProperties::GD_TOURIST: {
        strDifficulty = TRANS("Tourist");
      } break;

      case CSessionProperties::GD_EASY: {
        strDifficulty = TRANS("Easy");
      } break;

      default: {
        ASSERT(FALSE);
      }

      case CSessionProperties::GD_NORMAL: {
        strDifficulty = TRANS("Normal");
      } break;

      case CSessionProperties::GD_HARD: {
        strDifficulty = TRANS("Hard");
      } break;

      case CSessionProperties::GD_EXTREME: {
        strDifficulty = TRANS("Serious");
      } break;
    }
  }

  strKey.PrintF(";difficulty;%s", (const char*)strDifficulty);
  strOut += strKey;

  strKey.PrintF(";friendlyfire;%d", sp.sp_bFriendlyFire ? 0 : 1);
  strOut += strKey;

  /*
  strKey.PrintF(";weaponsstay;%d", sp.sp_bWeaponsStay ? 0 : 1);
  strOut += strKey;

  strKey.PrintF(";ammostays;%d", sp.sp_bAmmoStays ? 0 : 1);
  strOut += strKey;

  strKey.PrintF(";healthandarmorstays;%d", sp.sp_bHealthArmorStays? 0 : 1);
  strOut += strKey;

  strKey.PrintF(";allowhealth;%d", sp.sp_bAllowHealth ? 0 : 1);
  strOut += strKey;

  strKey.PrintF(";allowarmor;%d", sp.sp_bAllowArmor ? 0 : 1);
  strOut += strKey;

  strKey.PrintF(";infiniteammo;%d", sp.sp_bInfiniteAmmo ? 0 : 1);
  strOut += strKey;

  strKey.PrintF(";respawninplace;%d", sp.sp_bRespawnInPlace ? 0 : 1);
  strOut += strKey;

  if (sp.sp_bCooperative) {
    if (sp.sp_ctCredits < 0) {
      strKey.PrintF(";credits;infinite");
      strOut += strKey;

    } else if (sp.sp_ctCredits > 0) {
      strKey.PrintF(";credits;%d", sp.sp_ctCredits);
      strOut += strKey;

      strKey.PrintF(";credits_left;%d", sp.sp_ctCreditsLeft);
      strOut += strKey;
    }

  } else {
    if (sp.sp_bUseFrags && sp.sp_iFragLimit > 0) {
      strKey.PrintF(";fraglimit;%d", sp.sp_iFragLimit);
      strOut += strKey;
    }

    if (!sp.sp_bUseFrags && sp.sp_iScoreLimit > 0) {
      strKey.PrintF(";fraglimit;%d", sp.sp_iScoreLimit);
      strOut += strKey;
    }

    if (sp.sp_iTimeLimit > 0) {
      strKey.PrintF(";timelimit;%d", sp.sp_iTimeLimit);
      strOut += strKey;
    }
  }
  */

  strOut += gam_strGameAgentExtras;

  return strOut;
}

ULONG GetSpawnFlagsForGameType(INDEX iGameType)
{
  switch(iGameType)
  {
    default: {
      ASSERT(FALSE);
    }

    case CSessionProperties::GM_COOPERATIVE: {
      return SPF_COOPERATIVE;
    }

    case CSessionProperties::GM_SCOREMATCH: {
      return SPF_DEATHMATCH;
    }

    case CSessionProperties::GM_FRAGMATCH: {
      return SPF_DEATHMATCH;
    }

    case CSessionProperties::GM_TEAMDEATHMATCH: {
      return SPF_DEATHMATCH;
    }
  };
}

ULONG GetSpawnFlagsForGameTypeCfunc(void* pArgs)
{
  INDEX iGameType = NEXTARGUMENT(INDEX);
  return GetSpawnFlagsForGameType(iGameType);
}

extern void InitializeGameOptions()
{
  // General
  _pShell->DeclareSymbol("persistent user INDEX gam_ctMaxPlayers;", &gam_ctMaxPlayers);
  _pShell->DeclareSymbol("persistent user INDEX gam_bWaitAllPlayers;", &gam_bWaitAllPlayers);
  _pShell->DeclareSymbol("user INDEX gam_iQuickStartDifficulty;", &gam_iQuickStartDifficulty);
  _pShell->DeclareSymbol("user INDEX gam_iQuickStartMode;",       &gam_iQuickStartMode);
  _pShell->DeclareSymbol("user INDEX gam_bQuickStartMP;",       &gam_bQuickStartMP);
  _pShell->DeclareSymbol("persistent user INDEX gam_iStartDifficulty;", &gam_iStartDifficulty);
  _pShell->DeclareSymbol("persistent user INDEX gam_iStartMode;",       &gam_iStartMode);

  // Scorematch
  _pShell->DeclareSymbol("persistent user FLOAT gam_fManaTransferFactor;", &gam_fManaTransferFactor);
  _pShell->DeclareSymbol("persistent user INDEX gam_iInitialMana;",        &gam_iInitialMana);

  // Difficulty
  _pShell->DeclareSymbol("FLOAT gam_afEnemyMovementSpeed[5];", &gam_afEnemyMovementSpeed);
  _pShell->DeclareSymbol("FLOAT gam_afEnemyAttackSpeed[5];",   &gam_afEnemyAttackSpeed);
  _pShell->DeclareSymbol("FLOAT gam_afDamageStrength[5];",     &gam_afDamageStrength);
  _pShell->DeclareSymbol("FLOAT gam_afAmmoQuantity[5];",       &gam_afAmmoQuantity);
  _pShell->DeclareSymbol("persistent user FLOAT gam_fExtraEnemyStrength         ;", &gam_fExtraEnemyStrength          );
  _pShell->DeclareSymbol("persistent user FLOAT gam_fExtraEnemyStrengthPerPlayer;", &gam_fExtraEnemyStrengthPerPlayer );

  // Versus/Teamplay Limits
  _pShell->DeclareSymbol("persistent user INDEX gam_iScoreLimit;",  &gam_iScoreLimit);
  _pShell->DeclareSymbol("persistent user INDEX gam_iFragLimit;",   &gam_iFragLimit);
  _pShell->DeclareSymbol("persistent user INDEX gam_iGoalLimit;",   &gam_iGoalLimit);
  _pShell->DeclareSymbol("persistent user INDEX gam_iTimeLimit;",   &gam_iTimeLimit);

  // Common Flags
  _pShell->DeclareSymbol("persistent user INDEX gam_bCustomizedGame;",   &gam_bCustomizedGame);
  _pShell->DeclareSymbol("persistent user INDEX gam_bAllowHealthItems;",   &gam_bAllowHealthItems);
  _pShell->DeclareSymbol("persistent user INDEX gam_bAllowArmorItems;",    &gam_bAllowArmorItems);
  _pShell->DeclareSymbol("persistent user INDEX gam_bAllowWeaponItems;",   &gam_bAllowWeaponItems);
  _pShell->DeclareSymbol("persistent user INDEX gam_bAllowAmmoItems;",     &gam_bAllowAmmoItems);
  _pShell->DeclareSymbol("persistent user INDEX gam_bAllowAmmoPackItems;", &gam_bAllowAmmoPackItems);
  _pShell->DeclareSymbol("persistent user INDEX gam_bAllowPowerUpItems;",  &gam_bAllowPowerUpItems);
  _pShell->DeclareSymbol("persistent user INDEX gam_bAllowGadgetItems;",   &gam_bAllowGadgetItems);

  _pShell->DeclareSymbol("persistent user INDEX gam_bInfiniteAmmo;", &gam_bInfiniteAmmo);
  _pShell->DeclareSymbol("persistent user INDEX gam_bFriendlyFire;",   &gam_bFriendlyFire);

  // Common Options
  _pShell->DeclareSymbol("persistent user FLOAT gam_tmRespawnInvulnerability;", &gam_tmRespawnInvulnerability);
  _pShell->DeclareSymbol("persistent user FLOAT gam_tmRespawnDelay;", &gam_tmRespawnDelay);

  // Cooperative Flags
  _pShell->DeclareSymbol("persistent user INDEX gam_bPlayEntireGame;",  &gam_bPlayEntireGame);
  _pShell->DeclareSymbol("persistent user INDEX gam_bRespawnInPlace;",  &gam_bRespawnInPlace);
  _pShell->DeclareSymbol("persistent user INDEX gam_bUseLives;",        &gam_bUseLives);
  _pShell->DeclareSymbol("persistent user INDEX gam_bSharedLives;",     &gam_bSharedLives);
  _pShell->DeclareSymbol("persistent user INDEX gam_bSharedKeys;",      &gam_bSharedKeys);
  _pShell->DeclareSymbol("persistent user INDEX gam_bHealthStays;",     &gam_bHealthStays);
  _pShell->DeclareSymbol("persistent user INDEX gam_bArmorStays;",      &gam_bArmorStays);
  _pShell->DeclareSymbol("persistent user INDEX gam_bAmmoStays;",       &gam_bAmmoStays);
  _pShell->DeclareSymbol("persistent user INDEX gam_bWeaponsStay;",     &gam_bWeaponsStay);
  _pShell->DeclareSymbol("persistent user INDEX gam_bUseExtraEnemies;", &gam_bUseExtraEnemies);

  // Cooperative Options
  _pShell->DeclareSymbol("persistent user INDEX gam_iInitialLives;",        &gam_iInitialLives);
  _pShell->DeclareSymbol("persistent user INDEX gam_iInitialScoreForLife;", &gam_iInitialScoreForLife);

  // Versus Flags
  _pShell->DeclareSymbol("persistent user INDEX gam_bAutoRespawn;",  &gam_bAutoRespawn);
  _pShell->DeclareSymbol("persistent user INDEX gam_bDropWeapon;",  &gam_bDropWeapon);
  _pShell->DeclareSymbol("persistent user INDEX gam_bDropPowerUp;",  &gam_bDropPowerUp);

  // Blood & Gore
  _pShell->DeclareSymbol("persistent user INDEX gam_iBlood;", &gam_iBlood);
  _pShell->DeclareSymbol("persistent user INDEX gam_bGibs;",  &gam_bGibs);
}

CSessionProperties::CSessionProperties()
{
  Clear();
}

CSessionProperties::CSessionProperties(const CSessionProperties& other)
{
  Copy(other);
}

CSessionProperties::~CSessionProperties()
{
}

void CSessionProperties::Copy(const CGameObject &other)
{
  const CSessionProperties &spOther = static_cast<const CSessionProperties &>(other);
  
  sp_ctMaxPlayers = spOther.sp_ctMaxPlayers;
  sp_bWaitAllPlayers = spOther.sp_bWaitAllPlayers;
  sp_bQuickTest = spOther.sp_bQuickTest;
  sp_bCooperative = spOther.sp_bCooperative;
  sp_bSinglePlayer = spOther.sp_bSinglePlayer;
  sp_bVersus = spOther.sp_bVersus;
  sp_bUseFrags = spOther.sp_bUseFrags;

  sp_gmGameMode = spOther.sp_gmGameMode;

  sp_gdGameDifficulty = spOther.sp_gdGameDifficulty;
  sp_ulSpawnFlags = spOther.sp_ulSpawnFlags;
  sp_bMental = spOther.sp_bMental;

  sp_iScoreLimit = spOther.sp_iScoreLimit;
  sp_iFragLimit = spOther.sp_iFragLimit;
  sp_iGoalLimit = spOther.sp_iGoalLimit;
  sp_iTimeLimit = spOther.sp_iTimeLimit;
  
  sp_bTeamPlay = spOther.sp_bTeamPlay;
  sp_bFriendlyFire = spOther.sp_bFriendlyFire;
  sp_bInfiniteAmmo = spOther.sp_bInfiniteAmmo;

  sp_fEnemyMovementSpeed = spOther.sp_fEnemyMovementSpeed;
  sp_fEnemyAttackSpeed = spOther.sp_fEnemyAttackSpeed;
  sp_fDamageStrength = spOther.sp_fDamageStrength;
  sp_fAmmoQuantity = spOther.sp_fAmmoQuantity;
  sp_fAmmoPickupQuantity = spOther.sp_fAmmoPickupQuantity;
  sp_fExtraEnemyStrength = spOther.sp_fExtraEnemyStrength;
  sp_fExtraEnemyStrengthPerPlayer = spOther.sp_fExtraEnemyStrengthPerPlayer;

  sp_tmSpawnInvulnerability = spOther.sp_tmSpawnInvulnerability;

  sp_bEndOfGame = spOther.sp_bEndOfGame;

  sp_ulLevelsMask = spOther.sp_ulLevelsMask;

  sp_bCustomizedGame = spOther.sp_bCustomizedGame;
  sp_ulAllowedItemTypes = spOther.sp_ulAllowedItemTypes;
  sp_ulCoopFlags = spOther.sp_ulCoopFlags;
  sp_ctInitialLives = spOther.sp_ctInitialLives;
  sp_ctInitialScoreForLife = spOther.sp_ctInitialScoreForLife;

  sp_ulVersusFlags = spOther.sp_ulVersusFlags;
  sp_tmRespawnDelay = spOther.sp_tmRespawnDelay;

  sp_fManaTransferFactor = spOther.sp_fManaTransferFactor;
  sp_iInitialMana = spOther.sp_iInitialMana;

  sp_iBlood = spOther.sp_iBlood;
  sp_bGibs = spOther.sp_bGibs;
}

//! Get exact copy of the object.
CGameObject *CSessionProperties::Clone() const
{
  return new CSessionProperties(*this);
}

void CSessionProperties::Clear()
{
  sp_ctMaxPlayers = 0;
  sp_bWaitAllPlayers = FALSE;
  sp_bQuickTest = FALSE;
  sp_bCooperative = FALSE;
  sp_bSinglePlayer = FALSE;
  sp_bVersus = FALSE;
  sp_bUseFrags = FALSE;

  sp_gmGameMode = GM_COOPERATIVE;
  sp_gdGameDifficulty = CSessionProperties::GD_EASY;
  sp_ulSpawnFlags = 0;
  sp_bMental = FALSE;

  sp_iScoreLimit = 0;
  sp_iFragLimit = 0;
  sp_iGoalLimit = 0;
  sp_iTimeLimit = 0;

  sp_bTeamPlay = 0;
  sp_bFriendlyFire = 0;
  sp_bInfiniteAmmo = 0;
  
  sp_fEnemyMovementSpeed = 1.0f;
  sp_fEnemyAttackSpeed = 1.0f;
  sp_fDamageStrength = 1.0f;
  sp_fAmmoQuantity = 1.0f;
  sp_fAmmoPickupQuantity = 1.0f;
  sp_fExtraEnemyStrength = 0.0f;
  sp_fExtraEnemyStrengthPerPlayer = 0.0f;
  
  sp_tmSpawnInvulnerability = 0.0f;

  sp_bEndOfGame = FALSE;

  sp_ulLevelsMask = FALSE;

  sp_bCustomizedGame = FALSE;
  sp_ulAllowedItemTypes = 0xFFFFFFFF;

  sp_ulCoopFlags = 0;
  sp_ctInitialLives = 0;
  sp_ctInitialScoreForLife = 0;

  sp_ulVersusFlags = 0;
  sp_tmRespawnDelay = 0.0F;

  sp_fManaTransferFactor = 0.0F;
  sp_iInitialMana = 0;

  sp_iBlood = 0;
  sp_bGibs = FALSE;
}

UBYTE CSessionProperties::GetType() const
{
  return CResource::TYPE_INVALID;
}

void CSessionProperties::Read_t(CTStream *istrFile)
{
  INDEX iGameMode, iGameDifficulty, iVersion;
  
  (*istrFile) >> iVersion;
  
  (*istrFile) >> sp_ctMaxPlayers;
  (*istrFile) >> sp_bWaitAllPlayers;
  (*istrFile) >> sp_bQuickTest;
  (*istrFile) >> sp_bCooperative;
  (*istrFile) >> sp_bSinglePlayer;
  (*istrFile) >> sp_bVersus;
  (*istrFile) >> sp_bUseFrags;

  (*istrFile) >> iGameMode;
  (*istrFile) >> iGameDifficulty;
  (*istrFile) >> sp_ulSpawnFlags;
  (*istrFile) >> sp_bMental;

  sp_gmGameMode = (GameMode)iGameMode;
  sp_gdGameDifficulty = (CSessionProperties::GameDifficulty)iGameDifficulty;
  
  (*istrFile) >> sp_iScoreLimit;
  (*istrFile) >> sp_iFragLimit;
  (*istrFile) >> sp_iGoalLimit;
  (*istrFile) >> sp_iTimeLimit;

  (*istrFile) >> sp_bTeamPlay;
  (*istrFile) >> sp_bFriendlyFire;
  (*istrFile) >> sp_bInfiniteAmmo;
  
  (*istrFile) >> sp_fEnemyMovementSpeed;
  (*istrFile) >> sp_fEnemyAttackSpeed;
  (*istrFile) >> sp_fDamageStrength;
  (*istrFile) >> sp_fAmmoQuantity;
  (*istrFile) >> sp_fAmmoPickupQuantity;
  (*istrFile) >> sp_fExtraEnemyStrength;
  (*istrFile) >> sp_fExtraEnemyStrengthPerPlayer;
  
  (*istrFile) >> sp_tmSpawnInvulnerability;
  (*istrFile) >> sp_bEndOfGame;
  (*istrFile) >> sp_ulLevelsMask;
  
  (*istrFile) >> sp_bCustomizedGame;
  (*istrFile) >> sp_ulAllowedItemTypes;
  (*istrFile) >> sp_ulCoopFlags;
  (*istrFile) >> sp_ctInitialLives;
  (*istrFile) >> sp_ctInitialScoreForLife;
  (*istrFile) >> sp_ulVersusFlags;
  (*istrFile) >> sp_tmRespawnDelay;
  (*istrFile) >> sp_fManaTransferFactor;
  (*istrFile) >> sp_iInitialMana;
  (*istrFile) >> sp_iBlood;
  (*istrFile) >> sp_bGibs;
}

void CSessionProperties::Write_t(CTStream *ostrFile)
{
  const INDEX iGameMode = sp_gmGameMode;
  const INDEX iGameDifficulty = sp_gdGameDifficulty;
  
  (*ostrFile) << INDEX(0);

  (*ostrFile) << sp_ctMaxPlayers;
  (*ostrFile) << sp_bWaitAllPlayers;
  (*ostrFile) << sp_bQuickTest;
  (*ostrFile) << sp_bCooperative;
  (*ostrFile) << sp_bSinglePlayer;
  (*ostrFile) << sp_bVersus;
  (*ostrFile) << sp_bUseFrags;

  (*ostrFile) << iGameMode;
  (*ostrFile) << iGameDifficulty;
  (*ostrFile) << sp_ulSpawnFlags;
  (*ostrFile) << sp_bMental;

  (*ostrFile) << sp_iScoreLimit;
  (*ostrFile) << sp_iFragLimit;
  (*ostrFile) << sp_iGoalLimit;
  (*ostrFile) << sp_iTimeLimit;

  (*ostrFile) << sp_bTeamPlay;
  (*ostrFile) << sp_bFriendlyFire;
  (*ostrFile) << sp_bInfiniteAmmo;

  (*ostrFile) << sp_fEnemyMovementSpeed;
  (*ostrFile) << sp_fEnemyAttackSpeed;
  (*ostrFile) << sp_fDamageStrength;
  (*ostrFile) << sp_fAmmoQuantity;
  (*ostrFile) << sp_fAmmoPickupQuantity;
  (*ostrFile) << sp_fExtraEnemyStrength;
  (*ostrFile) << sp_fExtraEnemyStrengthPerPlayer;

  (*ostrFile) << sp_tmSpawnInvulnerability;
  (*ostrFile) << sp_bEndOfGame;
  (*ostrFile) << sp_ulLevelsMask;

  (*ostrFile) << sp_bCustomizedGame;
  (*ostrFile) << sp_ulAllowedItemTypes;
  (*ostrFile) << sp_ulCoopFlags;
  (*ostrFile) << sp_ctInitialLives;
  (*ostrFile) << sp_ctInitialScoreForLife;
  (*ostrFile) << sp_ulVersusFlags;
  (*ostrFile) << sp_tmRespawnDelay;
  (*ostrFile) << sp_fManaTransferFactor;
  (*ostrFile) << sp_iInitialMana;
  (*ostrFile) << sp_iBlood;
  (*ostrFile) << sp_bGibs;
}

CGameObject *CSessionPropertiesFactory::New() const
{
  return new CSessionProperties();
}