/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_CONTROLS_H
#define SE_INCL_CONTROLS_H

#define AXIS_ACTIONS_CT 9
#define SENSITIVITY_SLIDER_POSITIONS 25

// all available axis-actions are listed here
#define AXIS_MOVE_UD 0
#define AXIS_MOVE_LR 1
#define AXIS_MOVE_FB 2
#define AXIS_TURN_UD 3
#define AXIS_TURN_LR 4
#define AXIS_TURN_BK 5
#define AXIS_LOOK_UD 6
#define AXIS_LOOK_LR 7
#define AXIS_LOOK_BK 8

class CAxisAction
{
  public:
    INDEX aa_iAxisAction;       // to which axis this object referes to
    FLOAT aa_fSensitivity;      // percentage of maximum sensitivity (0..100)
    FLOAT aa_fDeadZone;         // percentage of deadzone (0..100)
    BOOL aa_bInvert;            // if controler's axis should be inverted
    BOOL aa_bRelativeControler; // if this is controler of type "relative"
    BOOL aa_bSmooth;            // if controler's axis should be smoothed
    // this is value for applying to "angle" or "movement" and it
    // is calculated from invert flag and sensitivity attributes
    // (i.e. for rotation colud be: AXIS_ROTATION_SPEED*sensitivity*(-1)(if should invert axis)
    FLOAT aa_fAxisInfluence;

    FLOAT aa_fLastReading; // last reading of this axis (for smoothing)
    FLOAT aa_fAbsolute;    // absolute value of the axis (integrated from previous readings)
};

class CButtonAction
{
  public:
    CListNode ba_lnNode;
    INDEX ba_iFirstKey;
    BOOL ba_bFirstKeyDown;
    INDEX ba_iSecondKey;
    BOOL ba_bSecondKeyDown;
    CTString ba_strName;
    CTString ba_strCommandLineWhenPressed;
    CTString ba_strCommandLineWhenReleased;

  public:
    // default constructor
    CButtonAction();

    // Assignment operator.
    virtual CButtonAction &operator=(const CButtonAction &baOriginal);

    virtual void Read_t( CTStream &istrm); // throw char*

    virtual void Write_t( CTStream &ostrm); // throw char*
};

/*
 * Class containing information concerning controls system
 */
class CControls
{
  public:
    // list of mounted button actions
    CListHead ctrl_lhButtonActions;
    // objects describing mounted controler's axis (mouse L/R, joy U/D) and their
    // attributes (sensitivity, intvertness, type (relative/absolute) ...)
    CAxisAction ctrl_aaAxisActions[ AXIS_ACTIONS_CT];
    FLOAT ctrl_fSensitivity;    // global sensitivity for all axes
    BOOL ctrl_bInvertLook;      // inverts up/down looking
    BOOL ctrl_bSmoothAxes;      // smooths axes movements

  public:
  // operations
    CControls(void); // default constructor

    virtual ~CControls(void); // default destructor

    // Assignment operator.
    virtual CControls &operator=(CControls &ctrlOriginal);

    // depending on axis attributes and type (rotation or translation), calculates axis
    // influence factors for all axis actions
    virtual void CalculateInfluencesForAllAxis(void);

    // get current reading of an axis
    virtual FLOAT GetAxisValue(INDEX iAxis);

    // check if these controls use any joystick
    virtual BOOL UsesJoystick(void);

    // switches button and axis action mounters to defaults
    virtual void SwitchAxesToDefaults(void);

    virtual void SwitchToDefaults(void);

    virtual void DoButtonActions(void);

    virtual void CreateAction(const CPlayerCharacter &pc, CPlayerAction &paAction, BOOL bPreScan);

    virtual CButtonAction &AddButtonAction(void);

    virtual void RemoveButtonAction(CButtonAction &baButtonAction);

    virtual void Load_t(CTFileName fnFile); // throw char *

    virtual void Save_t(CTFileName fnFile); // throw char *
};

#endif