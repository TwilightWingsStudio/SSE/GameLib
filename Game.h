/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef __GAME_H
#define __GAME_H 1

#include <Game/PlayerSettings.h>
#include <Game/SessionProperties.h>
#include <Game/Controls.h>
#include <Game/ConsoleMenu.h>
#include <Game/ComputerMenu.h>

#ifdef GAME_EXPORTS
  #define GAME_API __declspec(dllexport)
#else
  #define GAME_API __declspec(dllimport)
#endif

#define GAME_SHELL_VER "V014"

enum ConsoleState {
  CS_OFF,
  CS_ON,
  CS_TURNINGON,
  CS_TURNINGOFF,
  CS_ONINBACKGROUND,
  CS_TALK,
};

class CGameTimerHandler : public CTimerHandler
{
  public:
    /* This is called every TickQuantum seconds. */
    virtual void HandleTimer(void);
};

class CLocalPlayer
{
  public:
    // Attributes
    BOOL lp_bActive;
    INDEX lp_iPlayer;
    CPlayerSource *lp_pplsPlayerSource;
    UBYTE lp_ubPlayerControlsState[2048]; // current state of player controls that are local to the player

  public:
    //! Constructor.
    CLocalPlayer(void)
    {
      lp_pplsPlayerSource = NULL; 
      lp_bActive = FALSE; 
      memset(lp_ubPlayerControlsState, 0, sizeof(lp_ubPlayerControlsState)) ;
    };

    BOOL IsActive() const
    {
      return lp_bActive;
    }

    INDEX GetPlayer() const
    {
      return lp_iPlayer;
    }

    CPlayerSource *GetPlayerSource()
    {
      return lp_pplsPlayerSource;
    }
};

#define HIGHSCORE_COUNT 10

class CHighScoreEntry
{
  public:
    CTString hse_strPlayer;
    enum CSessionProperties::GameDifficulty hse_gdDifficulty;
    TIME hse_tmTime;
    INDEX hse_ctKills;
    INDEX hse_ctScore;

  public:
    CHighScoreEntry(void);

    const CTString &GetPlayerName() const
    {
      return hse_strPlayer;
    }

    enum CSessionProperties::GameDifficulty GetDifficulty() const
    {
      return hse_gdDifficulty;
    }

    TIME GetTime() const
    {
      return hse_tmTime;
    }

    INDEX GetKills() const
    {
      return hse_ctKills;
    }

    INDEX GetScore() const
    {
      return hse_ctScore;
    }
};

/*
 * Class responsible for handling game interface
 */
class CGame : public CResource
{
  public:
    CConsoleMenu &gm_ConsoleMenu;
    CComputerMenu &gm_ComputerMenu;

    enum ConsoleState gm_csConsoleState;
    enum ConsoleState gm_csComputerState;

    CTFileName gm_fnSaveFileName;

    CTString gam_strCustomLevel;
    CTString gam_strSessionName;
    CTString gam_strJoinAddress;
    CTString gam_strConsoleInputBuffer;

    CStaticArray<CTString> gm_astrAxisNames;

    CStaticArray<CHighScoreEntry> gm_ahseHighScores;
    INDEX gm_iLastSetHighScore;

    CStaticArray<CPlayerCharacter> gm_apcPlayers;
    CStaticArray<CControls> gm_actrlControls;
    CControls gm_ctrlControlsExtra;
    INDEX gm_iSinglePlayer;
    INDEX gm_iWEDSinglePlayer;

    enum SplitScreenCfg {
      SSC_DEDICATED = -2,
      SSC_OBSERVER = -1,
      SSC_PLAY1 = 0,
      SSC_PLAY2 = 1,
      SSC_PLAY3 = 2,
      SSC_PLAY4 = 3,
    };

    enum SplitScreenCfg gm_MenuSplitScreenCfg;
    enum SplitScreenCfg gm_StartSplitScreenCfg;
    enum SplitScreenCfg gm_CurrentSplitScreenCfg;

    // Attributes
    CGameTimerHandler m_gthGameTimerHandler;
    BOOL gm_bGameOn;
    BOOL gm_bMenuOn;       // set by serioussam.exe to notify that menu is active
    BOOL gm_bFirstLoading; // set by serioussam.exe to notify first loading
    BOOL gm_bProfileDemo;  // demo profiling required

    // network provider itself
    CNetworkProvider gm_npNetworkProvider;

    // network provider's description
    CTString gm_strNetworkProvider;
    // index of local player
    // (-1) if not active
    CStaticArray<INDEX> gm_aiMenuLocalPlayers;
    CStaticArray<INDEX> gm_aiStartLocalPlayers;
    // players that are currently playing on local machine (up to 4)
    CStaticArray<CLocalPlayer> gm_lpLocalPlayers;

  public:
    //! Constructor.
    CGame();

    // Operations
    void InitInternal(void);

    void EndInternal(void);

    BOOL StartProviderFromName(void);

    void SetupLocalPlayers( void);

    BOOL AddPlayers(void);

    void WriteHighScoreTable_t(CTStream &ostrm);

    void RecordHighScore(void);

    void ReadHighScoreTable_t(CTStream &istrm);

    void SaveThumbnail(const CTFileName &fnm);

    CTFileName GetQuickSaveName(BOOL bSave);

    void GameHandleTimer(void);

    virtual void LoadPlayersAndControls(void);

    virtual void SavePlayersAndControls(void);

    //! Read game settings from stream.
    void Read_t(CTStream *istrFile) override;  // throw char *

    //! Write game settings to stream.
    void Write_t(CTStream *ostrFile) override; // throw char *

    virtual void Load_t( void);

    virtual void Save_t( void);

    // set properties for a quick start session
    virtual void SetQuickStartSession(CGameObject *pProperties);

    // set properties for a single player session
    virtual void SetSinglePlayerSession(CGameObject *pProperties);

    // set properties for a multiplayer session
    virtual void SetMultiPlayerSession(CGameObject *pProperties);

    // game loop functions
  #define GRV_SHOWEXTRAS  (1L<<0)   // add extra stuff like console, weapon, pause
    virtual void GameRedrawView(CDrawPort *pdpDrawport, ULONG ulFlags);

    virtual void GameMainLoop(void);

    // console functions
    virtual CConsoleMenu *GetConsoleMenu();

    virtual enum ConsoleState ConsoleGetState() const;

    virtual void ConsoleSetState(ConsoleState csNew);

    virtual void ConsoleKeyDown(MSG msg);

    virtual void ConsoleChar(MSG msg);

    virtual void ConsoleRender(CDrawPort *pdpDrawport);

    virtual void ConsolePrintLastLines(CDrawPort *pdpDrawport);

    // computer functions
    virtual CComputerMenu *GetComputerMenu();

    virtual enum ConsoleState ComputerGetState() const;

    virtual void ComputerSetState(ConsoleState csNew);

    virtual void ComputerMouseMove(PIX pixX, PIX pixY);

    virtual void ComputerKeyDown(MSG msg);

    virtual void ComputerRender(CDrawPort *pdpDrawport);

    virtual void ComputerForceOff();


    // loading hook functions
    virtual void EnableLoadingHook(CDrawPort *pdpDrawport);

    virtual void DisableLoadingHook(void);


    // get default description for a game (for save games/demos)
    virtual CTString GetDefaultGameDescription(BOOL bWithInfo);


    // game start/end functions
    virtual BOOL NewGame(const CTString &strSessionName, const CTFileName &fnWorld, CGameObject *pProperties);

    virtual BOOL JoinGame(CNetworkSession &session);

    virtual BOOL LoadGame(const CTFileName &fnGame);

    virtual BOOL SaveGame(const CTFileName &fnGame);

    virtual void StopGame(void);

    virtual BOOL StartDemoPlay(const CTFileName &fnDemo);

    virtual BOOL StartDemoRec(const CTFileName &fnDemo);

    virtual void StopDemoRec(void);

    virtual INDEX GetPlayersCount(void);

    virtual INDEX GetLivePlayersCount(void);

    virtual const CTString &GetCustomLevel() const;

    virtual const CTString &GetSessionName() const;

    virtual const CTString &GetJoinAddress() const;

    virtual const CTString &GetConsoleInputBuffer() const;

    virtual const CTString &GetNetworkProvider() const;

    virtual CStaticArray<CTString> &GetAxisNames();

    virtual CStaticArray<CHighScoreEntry> &GetHighScores();

    virtual INDEX GetLastSetHighScoreIndex() const;

    virtual CStaticArray<CPlayerCharacter> &GetPlayers();

    virtual CStaticArray<CControls> &GetControls();

    virtual CControls &GetControlsExtra();

    virtual INDEX GetSinglePlayerIndex() const;

    virtual INDEX &GetSinglePlayerIndexRef();

    virtual CStaticArray<INDEX> &GetMenuLocalPlayers();

    virtual void SetMenuLocalPlayers(const CStaticArray<INDEX> &aiPlayers);

    virtual void ResetMenuLocalPlayers();

    virtual CStaticArray<INDEX> &GetStartLocalPlayers();

    virtual void SetStartLocalPlayers(const CStaticArray<INDEX> &aiPlayers);

    virtual void ResetStartLocalPlayers();

    virtual CStaticArray<CLocalPlayer> &GetLocalPlayers();

    virtual BOOL IsGameOn() const;

    virtual BOOL IsMenuOn() const;

    virtual BOOL IsFirstLoading() const;

    virtual enum SplitScreenCfg GetMenuSplitScreenCfg() const;

    virtual enum SplitScreenCfg GetStartSplitScreenCfg() const;

    virtual enum SplitScreenCfg GetCurrentSplitScreenCfg() const;

    virtual void SetCustomLevel(const CTString &strCustomLevel);

    virtual void SetSessionName(const CTString &strSessionName);

    virtual void SetJoinAddress(const CTString &strJoinAddress);

    virtual void SetNetworkProvider(const CTString &strNetworkProvider);

    virtual void SetMenuOn(BOOL bMenuOn);
    
    virtual void SetFirstLoading(BOOL bFirstLoading);

    virtual void SetMenuSplitScreenCfg(SplitScreenCfg cfg);

    virtual void SetStartSplitScreenCfg(SplitScreenCfg cfg);

    // printout and dump extensive demo profile report
    virtual CTString DemoReportFragmentsProfile( INDEX iRate);

    virtual CTString DemoReportAnalyzedProfile(void);

    // functions called from world editor
    virtual void Initialize(const CTFileName &fnGameSettings);

    virtual void End(void);

    virtual void QuickTest(const CTFileName &fnMapName, CDrawPort *pdpDrawport, CViewPort *pvpViewport);

    // interface rendering functions
    virtual void LCDInit(void);

    virtual void LCDEnd(void);

    virtual void LCDPrepare(FLOAT fFade);

    virtual void LCDSetDrawport(CDrawPort *pdp);

    virtual void LCDDrawBox(PIX pixUL, PIX pixDR, PIXaabbox2D &box, COLOR col);

    virtual void LCDScreenBox(COLOR col);

    virtual void LCDScreenBoxOpenLeft(COLOR col);

    virtual void LCDScreenBoxOpenRight(COLOR col);

    virtual void LCDRenderClouds1(void);

    virtual void LCDRenderClouds2(void);

            void LCDRenderCloudsForComp(void);

            void LCDRenderCompGrid(void);

    virtual void LCDRenderGrid(void);

    virtual void LCDDrawPointer(PIX pixI, PIX pixJ);

    virtual COLOR LCDGetColor(COLOR colDefault, const char *strName);

    virtual COLOR LCDFadedColor(COLOR col);

    virtual COLOR LCDBlinkingColor(COLOR col0, COLOR col1);


    // menu interface functions
    virtual void MenuPreRenderMenu(const char *strMenuName);

    virtual void MenuPostRenderMenu(const char *strMenuName);
};

#endif
